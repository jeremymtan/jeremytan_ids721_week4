use actix_files::Files;
use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;

// flips a coin using random number generator
async fn coinflip() -> impl Responder {
    let result = if rand::thread_rng().gen::<bool>() {
        "Heads"
    } else {
        "Tails"
    };

    HttpResponse::Ok().body(result)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            // have to sure your route goes first before serving other files
            .route("/coinflip", web::get().to(coinflip)) // Route to the coinflip function
            .service(Files::new("/", "./static/root/").index_file("index.html"))
    })
    .bind(("0.0.0.0", 50505))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use actix_web::{body::to_bytes, dev::Service, http, test, web, App, Error};

    use super::*;

    #[actix_web::test]
    async fn test_index() -> Result<(), Error> {
        let app = App::new().route("/coinflip", web::get().to(coinflip));
        let app = test::init_service(app).await;

        let req = test::TestRequest::get().uri("/coinflip").to_request();
        let resp = app.call(req).await?;

        assert_eq!(resp.status(), http::StatusCode::OK);
        Ok(())
    }
}
