[![pipeline status](https://gitlab.com/jeremymtan/jeremytan_ids721_week4/badges/master/pipeline.svg)](https://gitlab.com/jeremymtan/jeremytan_ids721_week4/-/commits/master)

## Purpose of Project 
The purpose of the project is to containerize a Rust Actix Web Service. I build a simple web app that will flip a coin for the user upon pressing a button in the web app. 

### Web App (hosted via a container)
`\ route`

![Screenshot_2024-02-17_at_4.44.28_PM](/uploads/16f1e2ab12eab8415a0c8dd6f3c07972/Screenshot_2024-02-17_at_4.44.28_PM.png)

`\coinflip route`

![Screenshot_2024-02-17_at_4.44.34_PM](/uploads/2799de0b679ebc66b7f858b3632084d6/Screenshot_2024-02-17_at_4.44.34_PM.png)

### Docker Image

![Screenshot_2024-02-17_at_4.46.55_PM](/uploads/43703b701096a988591d701ef4e4d86c/Screenshot_2024-02-17_at_4.46.55_PM.png)

### Docker Container 

![Screenshot_2024-02-17_at_4.56.15_PM](/uploads/ddcd8c13dfaeba062ec811d8ab951817/Screenshot_2024-02-17_at_4.56.15_PM.png)

### Preparation
1. run `cargo new <insert project name here>`
2. add depedenecies found in my `cargo.toml` file 
3. build your web app, ideally with better ui 
4. run your web app first before contanierizing via `cargo run` and do `make all` to check everything works fine (assuming you copy my Makefile)
5. after confirming it runs, make a `Dockerfile`
6. build docker image: `docker build -t <insert any name here> .`   
7. deploy docker container to make sure it works: `docker run -d -p 50505 <insert image name here>`
8. push and run cicd

### References
1. https://actix.rs/docs/getting-started
2. https://github.com/actix/examples
3. https://coursera.org/groups/rust-axum-greedy-coin-microservice-ormq0/
 


